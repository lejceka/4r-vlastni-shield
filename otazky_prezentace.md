# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala - **čistý čas** | 13 hodin |
| jak se mi to podařilo rozplánovat |Dobře, vše probíhalo, jak jsem to naplánoval |
| design zapojení | https://gitlab.spseplzen.cz/lejceka/4r-vlastni-shield/-/blob/main/dokumentace/design/navrh_rozlozeni.jpg |
| proč jsem zvolil tento design |Kvůli omezenému místu na shieldu |
| zapojení | https://gitlab.spseplzen.cz/lejceka/4r-vlastni-shield/-/blob/main/dokumentace/design/zapojeni.png |
| z jakých součástí se zapojení skládá |2X LED (1X Green LED, Red LED), Fotorezistor, Rotační enkodér, RGB LED pásek, Teploměr, Vodiče, 4X Rezistor |
| realizace | https://gitlab.spseplzen.cz/lejceka/4r-vlastni-shield/-/blob/main/dokumentace/fotky/IMG_20230927_083653.jpg |
| nápad, v jakém produktu vše propojit dohromady|  |
| co se mi povedlo | Design |
| co se mi nepovedlo/příště bych udělal/a jinak | Dokumentaci |
| zhodnocení celé tvorby | Myslím si, že výsledek je v pořádku a odpovídá zadání |